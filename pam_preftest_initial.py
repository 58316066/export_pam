#!/usr/bin/env python
# coding: utf-8
import mysql.connector
import pandas as pd
import numpy as np
import smtplib
import os.path
import sys
import getopt
import csv
import re
import boto3
import glob
import os
import string
import random
import datetime

from faker import Faker
from boto3.session import Session
from pathlib import Path
from src.env import ENV
from mysql.connector import Error
from datetime import *
from dateutil.relativedelta import relativedelta
from os import path


PROGRAM_NAME = os.path.basename(sys.argv[0])
fake = Faker()

def iecho(msg):
    print(datetime.today().strftime('[%Y-%m-%d][%H:%M:%S]: ')+str(msg))


def sql_pull(query, db='readonly'):
    """
    function for pulling database (db = readonly (default) or desktop)
    """
    if db == 'readonly':
        host = ENV.HOST_READ
        user = ENV.USER_READ
        passwd = ENV.PASS_READ
        port = ENV.PORT_READ
    elif db == 'desktop':
        host = ENV.HOST_WRITE
        user = ENV.USER_WRITE
        passwd = ENV.PASS_WRITE
        port = ENV.PORT_WRITE
    else:
        iecho('wrong db, must be either readonly or desktop')

    try:
        connection = mysql.connector.connect(
            host=host, port=port, user=user, passwd=passwd)
        if connection.is_connected():
            iecho('query executing...')
            df = pd.read_sql(query, connection)

    except Error as e:
        iecho("Error while connecting to MySQL")

    finally:
        if connection.is_connected():
            connection.close()
            iecho("MySQL connection is closed")

    return df


def del_df(dataframe):
    del dataframe


#################################################################################
# SET VARIABLE PROGRAM
#################################################################################

START_DATE = ENV.START_DATE
END_DATE = ENV.END_DATE

transaction_file = ENV.TRANSACTION_FILE
new_register_user_file = ENV.NEW_REGIS_USER_FILE
update_customer_file = ENV.UPDATE_CUSTOMER_FILE

output_path = ENV.EXPORT_PATH

Last_3_Days = ENV.Last_3_Days
Last_7_Days = ENV.Last_7_Days
Last_15_Days = ENV.Last_15_Days
Last_30_Days = ENV.Last_30_Days
Last_90_Days = ENV.Last_90_Days

# S3 SETUP
BUCKET_NAME = ENV.S3_BUCKET_NAME
AWSAccessKeyId = ENV.S3_ACCESSKEY_ID
AWSSecretKey = ENV.S3_SECRET_KEY
PATH_NAME = ENV.S3_PATH_NAME
list_filename = []
countdb_list=[]


iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
iecho(f'                      START PROCESS {PROGRAM_NAME}                          ')
iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
iecho(f'GENERATE VARIABLE')
iecho(f'  START_DATE         : {START_DATE} 00:00:00')
iecho(f'  END_DATE           : {END_DATE} 23:59:59')
iecho(f'  LAST_3_DAY         : {Last_3_Days}')
iecho(f'  LAST_7_DAY         : {Last_7_Days}')
iecho(f'  LAST_15_DAY        : {Last_15_Days}')
iecho(f'  LAST_30_DAY        : {Last_30_Days}')
iecho(f'  LAST_90_DAY        : {Last_90_Days}')
iecho(f'GENERATE FILE')
iecho(f'  EXPORT_FILE_FORMAT : {ENV.FILE_EXPORT}')
iecho(f'  OUTPUT PATH        : {output_path}/')


# Create a folder if it doesn't exist.
if not path.isdir(output_path):
    try:
        os.makedirs(output_path)
        os.chmod(output_path, 0o777)
    except OSError as error:
        iecho("Error: os.mkdir('" + output_path +
              "') please check permission "+error)
        exit(0)

# print(ENV.HOUR)


# In[ ]:


query = f"""
select
    "initial_import" as "event"
    ,now() as "created_at"
    ,cp.user_id as "customer"
    ,MD5(cp.first_name) as "firstname"
    ,MD5(cp.last_name) as "lastname"
    ,CONCAT(MD5(cp.first_name),"@gmail.com") as "email"
    ,CONCAT("+66",SUBSTRING(cp.mobile_no,2,5),"1234") as "sms" 
    ,cp.register_date as "register-date"
    ,cp.birth_date as "date-of-birth"
    ,substring(cp.birth_date, 6,2) AS "month-of-birth"
    ,cp.gender as "gender"
    ,"true" as "_allow_consent"
    ,"21wuXoX2IVaZntN5Lq11GLGqqZA" as "_consent_message_id"
    ,"1" as "_version"
    ,"true" as "_allow_terms_and_conditions"
    ,"true" as "_allow_privacy_overview"
    ,"true" as "_allow_email"
    ,"true" as "_allow_sms"
    ,"true" as "_allow_line"
    ,"true" as "_allow_facebook_messenger"
    ,"true" as "_allow_push_notification"
    ,"{ENV.DB_NAME}" as "_database"
from customer_profile.customer_profile cp where substring(cp.register_date,1,10) <= substring(DATE_SUB(now(),INTERVAL 1 DAY),1,10)
;
"""
iecho(query)
customer_update = sql_pull(query, db='readonly')
# countdb_list.append(len(summary_cust))
customer_update


# In[112]:


# first_name=[]
# last_name=[]
# mail=[]
# tel=[]
# for i in range(len(customer_update)):
#     first_name.append(fake.first_name())
#     last_name.append(fake.last_name())
#     mail.append(first_name[i]+str(i)+'@gmail.com')
#     tel.append('+669'+"".join(map(str, random.choices(string.digits, k=8))))


# # In[116]:


# cus_fake=pd.DataFrame({'firstname':first_name,'lastname':last_name,'email':mail,'sms':tel})
# cus_fake


# In[127]:


query2 = f"""select
cust.user_id as 'customer',
COUNT(DISTINCT (CASE WHEN trans.order_date between '{Last_3_Days} 00:00:00' and '{END_DATE} 23:59:59' THEN trans.order_id END)) AS 'last-3-days-txn-count',
COUNT(DISTINCT (CASE WHEN trans.order_date between '{Last_7_Days} 00:00:00' and '{END_DATE} 23:59:59' THEN trans.order_id END)) AS 'last-7-days-txn-count',
COUNT(DISTINCT (CASE WHEN trans.order_date between '{Last_15_Days} 00:00:00' and '{END_DATE} 23:59:59' THEN trans.order_id END)) AS 'last-15-days-txn-count',
COUNT(DISTINCT (CASE WHEN trans.order_date between '{Last_30_Days} 00:00:00' and '{END_DATE} 23:59:59' THEN trans.order_id END)) AS 'last-30-days-txn-count',
COUNT(DISTINCT (CASE WHEN trans.order_date between '{Last_90_Days} 00:00:00' and '{END_DATE} 23:59:59' THEN trans.order_id END)) AS 'last-90-days-txn-count',
AVG(trans.total_amount) as "lifetime-avg-ticket-size",
SUM(trans.total_amount) as "lifetime-value",
COUNT(trans.order_id) as "lifetime-txn-count",
COUNT(DISTINCT (CASE WHEN info.promo_code is not null THEN trans.order_id END)) as "lifetime-promo-count",
MIN(trans.order_date) as "first-transaction",
(SELECT MAX(tb1.order_date) FROM ordering.order_transaction tb1 where tb1.user_id = cust.user_id AND tb1.order_status='complete' AND tb1.order_date < MAX(trans.order_date)) as "second-latest-transaction",
MAX(trans.order_date) as "latest-transaction",
COUNT(distinct shop.merchant_id) as "no-unique-merchants"
FROM customer_profile.customer_profile cust
left join ordering.order_transaction trans on cust.user_id = trans.user_id
left join ordering.order_transaction_info info on trans.order_id = info.order_id
left join merchant.shop shop on trans.shop_id = shop.shop_id
WHERE trans.order_status = 'COMPLETE'
and substring(trans.order_date,1,10) <= substring(DATE_SUB(now(),INTERVAL 1 DAY),1,10)
group by cust.user_id;
"""
iecho(query2)
customer_amount = sql_pull(query2, db='readonly')
# countdb_list.append(len(summary_cust))
customer_amount


# In[129]:


customer_profile1 = customer_update.merge(customer_amount,how='left', left_on='customer', right_on='customer')
customer_profile1


# In[142]:


# customer_profile2 = customer_profile1.merge(cus_fake, left_index=True,right_index=True)
customer_profile2 = customer_profile1
customer_profile2 = customer_profile2[['event',
'created_at',
'customer',
'register-date',
'firstname',
'lastname',
'email',
'sms',
'date-of-birth',
'month-of-birth',
'gender',
'_allow_consent',
'_consent_message_id',
'_version',
'_allow_terms_and_conditions',
'_allow_privacy_overview',
'_allow_email',
'_allow_sms',
'_allow_line',
'_allow_facebook_messenger',
'_allow_push_notification',
'last-3-days-txn-count',
'last-7-days-txn-count',
'last-15-days-txn-count',
'last-30-days-txn-count',
'last-90-days-txn-count',
'lifetime-avg-ticket-size',
'lifetime-value',
'lifetime-txn-count',
'lifetime-promo-count',
'first-transaction',
'second-latest-transaction',
'latest-transaction',
'no-unique-merchants',
'_database']]
customer_profile2


# In[3]:


# customer_profile2.to_csv(f'{output_path}/customer_initial_import{datetime.now():%Y%m%d_%H%M%S}.csv', encoding='UTF-8',float_format='%.0f', quoting=csv.QUOTE_ALL, quotechar='"',
#                     index=False, sep=",", escapechar='\\')


# In[145]:


customer_profile2[['last-3-days-txn-count',
'last-7-days-txn-count',
'last-15-days-txn-count',
'last-30-days-txn-count',
'last-90-days-txn-count',
'lifetime-avg-ticket-size',
'lifetime-value',
'lifetime-txn-count',
'lifetime-promo-count',
'no-unique-merchants']] = customer_profile2[['last-3-days-txn-count',
'last-7-days-txn-count',
'last-15-days-txn-count',
'last-30-days-txn-count',
'last-90-days-txn-count',
'lifetime-avg-ticket-size',
'lifetime-value',
'lifetime-txn-count',
'lifetime-promo-count',
'no-unique-merchants']].fillna(value=0)
customer_profile2


# In[128]:


# customer_amount.to_csv(f'OUTPUT_PREFTES/all_customer_trans_{datetime.now():%Y%m%d}.csv', encoding='UTF-8',float_format='%.0f', quoting=csv.QUOTE_ALL, quotechar='"',
#                     index=False, sep=",", escapechar='\\')
# cus_fake.to_csv(f'OUTPUT_PREFTES/fake_customer_{datetime.datetime.now():%Y%m%d}.csv', encoding='UTF-8', quoting=csv.QUOTE_ALL, quotechar='"',
#                     index=False, sep=",", escapechar='\\')
# customer_update.to_csv(f'OUTPUT_PREFTES/all_customer_{datetime.now():%Y%m%d}.csv', encoding='UTF-8',float_format='%.0f', quoting=csv.QUOTE_ALL, quotechar='"',
#                     index=False, sep=",", escapechar='\\')


# In[146]:


iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
iecho('                                EXPORT CSV                                   ')
iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")

def export_file(df_export=['']):
    number_file=1
    file_name_conf = ENV.FILE_EXPORT_NIIT_1 # 
    for i, df in enumerate(df_export):
        iecho(f'index         : {i}')
        # print(type(df))
        #iecho(f'data frame name : {df}')

        start = 0
        off_set = 50000

        data = df[start : off_set]
        # row = len(data)
        while len(data) != 0:
            iecho('off_set_start :'+str(start))
            iecho('off_set_end   :'+str(off_set))
            iecho('len data      :'+ str(len(data)))

            # file_name = ENV.FILE_EXPORT_NIIT_1.replace('xxx',str(number_file))
            if number_file == 21:
                print(f"number of file is : {number_file}")
                file_name_conf = ENV.FILE_EXPORT_NIIT_2
                # do_something_good()
            elif number_file == 41:
                print(f"number of file is : {number_file}")
                file_name_conf = ENV.FILE_EXPORT_NIIT_3
            # else:
            #     print("Code not found")
            # if number_file in [21,41,51]:
            #     file_name = ENV.FILE_EXPORT_NIIT_2
                # pass

            file_name = file_name_conf.replace('xxx',str(number_file))
            iecho(f'start csv writer : {file_name}')
            data.to_csv(f'{output_path}/{file_name}', encoding='UTF-8', float_format='%.0f', quoting=csv.QUOTE_ALL, quotechar='"',
                    index=False, sep=",", escapechar='\\')

            list_filename.append(file_name)
            countdb_list.append(len(data))
            number_file+=1
            start=off_set
            off_set=off_set+50000
            data = df[start : off_set]

export_file([customer_profile2])


CORRECT_FILE = []
def validate_rowcount(list_filename, countdb_list):
    """
    Funcion validate number of row between (base table) and (export file)
    """
    iecho('validate rowcount')
    for i in range(len(list_filename)):

        filename = list_filename[i]
        COUNTBASETABLE = countdb_list[i]

        iecho(f'CONFIRM_COLUMN : {CONFIRM_COLUMN[i]}')
        if not CONFIRM_COLUMN[i]:
            iecho(f'FILENAME: {filename}')
            iecho(f'   ERROR: Can''t write control file becoure Count column missmach ')
            continue

        with open(f'{output_path}/{filename}', encoding="utf8") as f:
            for i, l in enumerate(f):
                pass
            ROWCOUNT=i

        if (COUNTBASETABLE != ROWCOUNT):
            iecho(f'FILENAME: {filename}')
            iecho(f'   ERROR: Record count on base table <> export data file')
            iecho(f'        :  Count base table = {COUNTBASETABLE}')
            iecho(f'        :  Count export file = {ROWCOUNT}')
            continue

        CORRECT_FILE.append(f'{output_path}/{str(filename)}')




def validate_column(list_filename):
    """
    Function validate column txt file
    """
    #append number of header column to list
    num_of_header = []
    global CONFIRM_COLUMN
    CONFIRM_COLUMN = []
    for file in list_filename:
        with open(f'{output_path}/{file}', encoding="utf8") as f:
            reader = csv.reader(f, delimiter=",", quoting=csv.QUOTE_ALL,
                                quotechar='"', escapechar='\\', skipinitialspace=True)
            first_row = next(reader)
            num_cols = len(first_row)
            iecho(f'{file}: {num_cols}')
            num_of_header.append(num_cols)
    num_of_header

    #validate number of data column
    for i in range(len(list_filename)):
        file = list_filename[i]
        num_header = num_of_header[i]

        with open(f'{output_path}/{file}', encoding="utf8") as f:
            reader = csv.reader(f, delimiter=",", quoting=csv.QUOTE_ALL,
                                quotechar='"', escapechar='\\', skipinitialspace=True)
            for row in reader:
                num_cols_data = len(row)
                if (num_cols_data != num_header):
                    iecho(f'FILENAME: {file}')
                    iecho(f'   ERROR: Column count on header <> column data')
                    iecho(f'        :  Count column on header = {num_header}')
                    iecho(f'        :  Count column on data = {num_cols_data}')
                    iecho(row)
                    CONFIRM_COLUMN.append(False)
                    break
        CONFIRM_COLUMN.append(True)


validate_column(list_filename)
validate_rowcount(list_filename, countdb_list)


def listfile():
    session = Session(aws_access_key_id=AWSAccessKeyId,
                      aws_secret_access_key=AWSSecretKey)
    s3 = session.resource('s3')
    bucket = s3.Bucket(BUCKET_NAME)
    files_in_s3 = bucket.objects.all()
    for file in files_in_s3:
        iecho(file.key)


def makefolder(foldername):
    s3 = boto3.client('s3')
    folder_name = foldername
    s3.put_object(Bucket=BUCKET_NAME, Key=(folder_name+'/'))


def uploadfile(content_file: None):
    session = Session(aws_access_key_id=AWSAccessKeyId,
                      aws_secret_access_key=AWSSecretKey)
    s3 = session.client('s3')

    for filename in content_file:
        key = "%s" % (os.path.basename(filename))
        iecho(f'Putting {filename} as {PATH_NAME}/{key}')
        s3.upload_file(filename, BUCKET_NAME, f'{PATH_NAME}/{key}')

    # for filename in content_file:
    #     os.remove(filename)


content_file = []
for file in CORRECT_FILE:
    apppath = str(Path().absolute())
    content_file.append(apppath+"/"+file)
# iecho(content_file)

iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
iecho('                            UPLOAD TO S3 BUCKET                              ')
iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
uploadfile(content_file)

iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
iecho('                          LIST FILE FROM S3 BUCKET                           ')
iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
listfile()

iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
iecho('                                END PROCESS                                  ')
iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
