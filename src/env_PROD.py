from datetime import *
import re
from time import strftime

class ENV:
    # env db tableau_readonly
    HOST_READ = 'rbh-prod-aurora-reader-db.rbh.internal'
    USER_READ = 'tableau_readonly'
    PASS_READ = 'Tabread1234!'
    PORT_READ = 3999

    # env db tableau_desktop
    HOST_WRITE = 'rbh-prod-my-tableau-db.rbh.internal'
    USER_WRITE = 'tableau_desktop'
    PASS_WRITE = 'Desktop1234!'
    PORT_WRITE = 3999

    # env export_path
    EXPORT_PATH = 'OUTPUT_FILE'

    # env file name
    TODAY = datetime.today()

    #################################### อย่าลืมเอา FIX date ออก ####################################
    # TODAY = datetime.strptime("2022-01-23", '%Y-%m-%d')
    # TODAY = datetime.today().strftime('%Y%m%d')

    # last_hour_date_time = datetime.today() + timedelta(hours = 1)
    # print(last_hour_date_time.strftime('%H'))

    HOUR = (TODAY + timedelta(hours = 1)).strftime('%H')
    
    DAY_1 = TODAY + timedelta(hours = 1)
    DAY_2 = DAY_1 + timedelta(hours = 8)
    DAY_3 = DAY_2 + timedelta(hours = 8)
    HOUR_1 = DAY_1.strftime('%H')
    HOUR_2 = DAY_2.strftime('%H')
    HOUR_3 = DAY_3.strftime('%H')

    # replace time eg. '01' to '1'
    if re.search('^0', HOUR):
        HOUR = HOUR.replace('0','')
    if re.search('^0', HOUR_1):
        HOUR_1 = HOUR_1.replace('0','')
    if re.search('^0', HOUR_2):
        HOUR_2 = HOUR_2.replace('0','')
    if re.search('^0', HOUR_3):
        HOUR_3 = HOUR_3.replace('0','')

    TRANSACTION_FILE = f'{TODAY:%Y%m%d}_{HOUR}_filetracker_1.csv.MAIN'
    NEW_REGIS_USER_FILE = f'{TODAY:%Y%m%d}_{HOUR}_filetracker_2.csv.MAIN'
    UPDATE_CUSTOMER_FILE = f'{TODAY:%Y%m%d}_{HOUR}_filetracker_3.csv.MAIN'
    FILE_EXPORT = f'{TODAY:%Y%m%d}_{HOUR}_filetracker_xxx.csv'

    FILE_EXPORT_NIIT_1 = f'{DAY_1:%Y%m%d}_{HOUR_1}_filetracker_xxx.csv'
    FILE_EXPORT_NIIT_2 = f'{DAY_2:%Y%m%d}_{HOUR_2}_filetracker_xxx.csv'
    FILE_EXPORT_NIIT_3 = f'{DAY_3:%Y%m%d}_{HOUR_3}_filetracker_xxx.csv'

    LIST_FILE = [TRANSACTION_FILE,NEW_REGIS_USER_FILE,UPDATE_CUSTOMER_FILE]

    # env date time
    yesterday = TODAY - timedelta(days=1)

    START_DATE = yesterday.strftime('%Y-%m-%d')
    END_DATE = yesterday.strftime('%Y-%m-%d')

    Last_3_Days = (TODAY - timedelta(days=3)).strftime('%Y-%m-%d')
    Last_7_Days = (TODAY - timedelta(days=7)).strftime('%Y-%m-%d')
    Last_15_Days = (TODAY - timedelta(days=15)).strftime('%Y-%m-%d')
    Last_30_Days = (TODAY - timedelta(days=30)).strftime('%Y-%m-%d')
    Last_90_Days = (TODAY - timedelta(days=90)).strftime('%Y-%m-%d')

    # S3 SETUP 
    S3_BUCKET_NAME = "pam-rbh-prod"
    S3_ACCESSKEY_ID = 'AKIA2M35QJBO62XLORNS'
    S3_SECRET_KEY = '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'
    # S3_ENV = 'pt'  # dev/qa/pt
    S3_PATH_NAME = f'import/schedule'
    # DB_NAME = "rbh-perf-test"
    DB_NAME = "rbh-consumer"
    CONSENT_MSG_ID = "24RpiowYojE23ZD1Wrin4svEl97"

    # Arthena SETUP
    ATHENA_ACCESSKEY_ID = "AKIAX74AEFLZ3CW2O5QE"
    ATHENA_SECRET_KEY = "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"
    ATHENA_AWS_REGION = "ap-southeast-1"
    ATHENA_DB_NAME = "nont"
    ATHENA_TABLE_NAME = "nont_robinhood_live_flattable_v4"