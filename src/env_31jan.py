from datetime import *
import re

class ENV:
    # env db tableau_readonly
    HOST_READ = 'rbh-prod-aurora-reader-db.rbh.internal'
    USER_READ = 'tableau_readonly'
    PASS_READ = 'Tabread1234!'
    PORT_READ = 3999

    # env db tableau_desktop
    HOST_WRITE = 'rbh-prod-my-tableau-db.rbh.internal'
    USER_WRITE = 'tableau_desktop'
    PASS_WRITE = 'Desktop1234!'
    PORT_WRITE = 3999

    # env export_path
    EXPORT_PATH = 'OUTPUT_FILE'

    # env file name
    TODAY = datetime.today().strftime('%Y%m%d')

    # last_hour_date_time = datetime.today() + timedelta(hours = 1)
    # print(last_hour_date_time.strftime('%H'))

    HOUR = (datetime.today() + timedelta(hours = 1)).strftime('%H')
    
    DAY_1 = datetime.today() + timedelta(hours = 1)
    DAY_2 = datetime.today() + timedelta(hours = 9)
    DAY_3 = datetime.today() + timedelta(hours = 17)
    HOUR_1 = DAY_1.strftime('%H')
    HOUR_2 = DAY_2.strftime('%H')
    HOUR_3 = DAY_3.strftime('%H')

    # replace time eg. '01' to '1'
    if re.search('^0', HOUR):
        HOUR = HOUR.replace('0','')
    if re.search('^0', HOUR_1):
        HOUR_1 = HOUR_1.replace('0','')
    if re.search('^0', HOUR_2):
        HOUR_2 = HOUR_2.replace('0','')
    if re.search('^0', HOUR_3):
        HOUR_3 = HOUR_3.replace('0','')

    TRANSACTION_FILE = f'{TODAY}_{HOUR}_filetracker_1.csv.MAIN'
    NEW_REGIS_USER_FILE = f'{TODAY}_{HOUR}_filetracker_2.csv.MAIN'
    UPDATE_CUSTOMER_FILE = f'{TODAY}_{HOUR}_filetracker_3.csv.MAIN'
    FILE_EXPORT = f'{TODAY}_{HOUR}_filetracker_xxx.csv'

    FILE_EXPORT_NIIT_1 = f'{DAY_1:%Y%m%d}_{DAY_1:%H}_filetracker_xxx.csv'
    FILE_EXPORT_NIIT_2 = f'{DAY_2:%Y%m%d}_{DAY_2:%H}_filetracker_xxx.csv'
    FILE_EXPORT_NIIT_3 = f'{DAY_3:%Y%m%d}_{DAY_3:%H}_filetracker_xxx.csv'

    LIST_FILE = [TRANSACTION_FILE,NEW_REGIS_USER_FILE,UPDATE_CUSTOMER_FILE]

    # env date time
    yesterday = datetime.today() - timedelta(days=1)

    START_DATE = yesterday.strftime('%Y-%m-%d')
    END_DATE = yesterday.strftime('%Y-%m-%d')

    Last_3_Days = (datetime.today() - timedelta(days=3)).strftime('%Y-%m-%d')
    Last_7_Days = (datetime.today() - timedelta(days=7)).strftime('%Y-%m-%d')
    Last_15_Days = (datetime.today() - timedelta(days=15)).strftime('%Y-%m-%d')
    Last_30_Days = (datetime.today() - timedelta(days=30)).strftime('%Y-%m-%d')
    Last_90_Days = (datetime.today() - timedelta(days=90)).strftime('%Y-%m-%d')

    # S3 SETUPz
    S3_BUCKET_NAME = "pam-rbh-stg"
    S3_ACCESSKEY_ID = 'AKIASSLEPJZPYS2Z2X7V'
    S3_SECRET_KEY = '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'
    S3_ENV = 'pt'  # dev/qa/pt
    S3_PATH_NAME = f'{S3_ENV}/import/schedule'
    # DB_NAME = "rbh-perf-test"
    DB_NAME = "rbh-pt-consumer"