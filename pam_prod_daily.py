#!/usr/bin/env python
# coding: utf-8
import mysql.connector
import pandas as pd
import numpy as np
import smtplib
import os.path
import sys
import getopt
import csv
import re
import boto3
import glob
import os


from boto3.session import Session
from pathlib import Path
from src.env_PROD import ENV
from mysql.connector import Error
from datetime import *
from dateutil.relativedelta import relativedelta
from os import path

PROGRAM_NAME = os.path.basename(sys.argv[0])


def iecho(msg):
    print(datetime.today().strftime('[%Y-%m-%d][%H:%M:%S]: ')+str(msg))


def sql_pull(query, db='readonly'):
    """
    function for pulling database (db = readonly (default) or desktop)
    """
    if db == 'readonly':
        host = ENV.HOST_READ
        user = ENV.USER_READ
        passwd = ENV.PASS_READ
        port = ENV.PORT_READ
    elif db == 'desktop':
        host = ENV.HOST_WRITE
        user = ENV.USER_WRITE
        passwd = ENV.PASS_WRITE
        port = ENV.PORT_WRITE
    else:
        iecho('wrong db, must be either readonly or desktop')

    try:
        connection = mysql.connector.connect(
            host=host, port=port, user=user, passwd=passwd)
        if connection.is_connected():
            iecho('query executing...')
            df = pd.read_sql(query, connection)

    except Error as e:
        iecho("Error while connecting to MySQL")

    finally:
        if connection.is_connected():
            connection.close()
            iecho("MySQL connection is closed")

    return df


def del_df(dataframe):
    del dataframe


#################################################################################
# SET VARIABLE PROGRAM
#################################################################################

START_DATE = ENV.START_DATE
END_DATE = ENV.END_DATE

transaction_file = ENV.TRANSACTION_FILE
new_register_user_file = ENV.NEW_REGIS_USER_FILE
update_customer_file = ENV.UPDATE_CUSTOMER_FILE

output_path = ENV.EXPORT_PATH

Last_3_Days = ENV.Last_3_Days
Last_7_Days = ENV.Last_7_Days
Last_15_Days = ENV.Last_15_Days
Last_30_Days = ENV.Last_30_Days
Last_90_Days = ENV.Last_90_Days

# S3 SETUP
BUCKET_NAME = ENV.S3_BUCKET_NAME
AWSAccessKeyId = ENV.S3_ACCESSKEY_ID
AWSSecretKey = ENV.S3_SECRET_KEY
PATH_NAME = ENV.S3_PATH_NAME
list_filename = []
countdb_list=[]


iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
iecho(f'                      START PROCESS {PROGRAM_NAME}                          ')
iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
iecho(f'GENERATE VARIABLE')
iecho(f'  START_DATE         : {START_DATE} 00:00:00')
iecho(f'  END_DATE           : {END_DATE} 23:59:59')
iecho(f'  LAST_3_DAY         : {Last_3_Days}')
iecho(f'  LAST_7_DAY         : {Last_7_Days}')
iecho(f'  LAST_15_DAY        : {Last_15_Days}')
iecho(f'  LAST_30_DAY        : {Last_30_Days}')
iecho(f'  LAST_90_DAY        : {Last_90_Days}')
iecho(f'GENERATE FILE')
iecho(f'  EXPORT_FILE_FORMAT : {ENV.FILE_EXPORT}')
iecho(f'  OUTPUT PATH        : {output_path}/')


# Create a folder if it doesn't exist.
if not path.isdir(output_path):
    try:
        os.makedirs(output_path)
        os.chmod(output_path, 0o777)
    except OSError as error:
        iecho("Error: os.mkdir('" + output_path +
              "') please check permission "+error)
        exit(0)


# Order Transaction
query = f"""
SELECT  
    "order_transaction" as "event"
    ,trans.order_date as created_at
    ,trans.order_id
    ,trans.user_id as customer
    ,shop.merchant_id as merchant_id
    ,trans.shop_id
    ,MD5(REPLACE(REPLACE(trans.shop_name,'\r',''),'\n','')) AS shop_name
    ,MD5(REPLACE(REPLACE(trans.shop_address,'\r',''),'\n','')) AS shop_address
    ,address.address_subdistrict AS shop_subdistrict
    ,address.address_district AS shop_district
    ,address.address_province AS shop_province
    ,address.address_postal_code AS shop_postal_code
    ,info.payment_type
    ,info.card_type
    ,info.payment_channel
    ,info.promo_code
    ,trans.item_quantity
    ,info.order_amount
    ,info.delivery_original_amt
    ,info.delivery_estimate_amt
    ,info.discount_amount
    ,trans.order_status
    ,trans.current_state
    ,trans.current_status
    ,DATE_FORMAT(trans.order_date, '%Y%m%d%H%i%s') AS order_date
    ,DATE_FORMAT(trans.order_date, '%W') AS date_of_week
    ,CASE WHEN DAYOFWEEK(trans.order_date) in (1,7) THEN "Weekend" ELSE "Weekday" END AS "weekend_or_weekday"
    ,DATE_FORMAT(trans.order_date, '%Y%m%d') AS date
    ,DATE_FORMAT(trans.order_date, '%H%i%s') AS time
    ,"{ENV.DB_NAME}" as _database
FROM ordering.order_transaction trans 
join ordering.order_transaction_info info on trans.order_id = info.order_id
join merchant.shop shop on trans.shop_id = shop.shop_id
join merchant.address address on shop.address_id = address.address_id
WHERE  trans.order_date between '""" + START_DATE + """ 00:00:00' AND '""" + END_DATE + """ 23:59:59'


;
"""
iecho(query)
order_trans = sql_pull(query, db='readonly')
# countdb_list.append(len(order_trans))
orders = list(order_trans['order_id'])

query = """
SELECT  trans_location.order_id AS order_id
    ,trans_location.cust_subdistrict AS delivery_subdistrict
    ,trans_location.cust_district AS delivery_district
    ,trans_location.cust_province AS delivery_province
FROM tableaudb.staging_order_transaction_location trans_location
WHERE  trans_location.order_id in """+ str(tuple(orders))+"""
;
"""
iecho(query[:1500])
trans_location = sql_pull(query, db='desktop')
# trans_location


df = order_trans.merge(trans_location, how='left', on='order_id')

del order_trans
del trans_location
# df


columns=['event','created_at', 'order_id', 'customer', 'merchant_id', 'shop_id', 'shop_name', 'shop_address',
       'shop_subdistrict', 'shop_district', 'shop_province',
       'shop_postal_code', 'delivery_subdistrict', 'delivery_district', 
        'delivery_province', 'payment_type', 'card_type', 'payment_channel',
       'promo_code', 'item_quantity', 'order_amount', 'delivery_original_amt',
       'delivery_estimate_amt', 'discount_amount', 'order_status', 'current_state', 'current_status','order_date', 
       'date_of_week', 'weekend_or_weekday','date', 'time','_database']
order_transaction = df.reindex(columns=columns)
del df
order_transaction


# New Register User
query = f"""
SELECT    
    "new_register_user" as event
    ,cust.register_date AS created_at
    ,cust.user_id AS customer
    ,cust.user_status AS user_status
    ,cust.title AS title
    ,MD5(cust.first_name) AS firstname
    ,MD5(cust.middle_name) AS middlename
    ,MD5(cust.last_name) AS lastname
    ,CONCAT(MD5(cust.first_name),"@gmail.com") as "email"
    ,CONCAT("+66",SUBSTRING(cust.mobile_no,2,5),"1234") as "sms" 
    ,cust.birth_date AS birth_date
    ,cust.tc_accept_date AS tc_accept_date 
    ,cust.tc_accept_version AS tc_accept_version
    ,cust.gender AS gender
    ,"true" as "_allow_consent"
    ,"21wuXoX2IVaZntN5Lq11GLGqqZA" as "_consent_message_id"
    ,"1" as "_version"
    ,"true" as "_allow_terms_and_conditions"
    ,"true" as "_allow_privacy_overview"
    ,"true" as "_allow_email"
    ,"true" as "_allow_sms"
    ,"true" as "_allow_line"
    ,"true" as "_allow_facebook_messenger"
    ,"true" as "_allow_push_notification"
    ,cust.language AS language
    ,cust.device_id AS device_id
    ,cust.register_date AS "register-date"
    ,cust.update_date AS update_date
    ,"{ENV.DB_NAME}" as _database
FROM customer_profile.customer_profile cust 
WHERE cust.register_date between '""" + START_DATE + """ 00:00:00' AND '""" + END_DATE + """ 23:59:59' ;
"""
iecho(query)
new_user = sql_pull(query, db='readonly')
# countdb_list.append(len(new_user))
new_user

# Update Customer
query = f"""
SELECT  
"update_customer" as "event",
MAX(trans.order_date)as "created_at",
cust.user_id as "customer",
cust.register_date as "register-date",
MD5(cust.first_name) as "firstname",
MD5(cust.last_name) as "lastname",
CONCAT(MD5(cust.first_name),"@gmail.com") as "email",
CONCAT("+66",SUBSTRING(cust.mobile_no,2,5),"1234") as "sms" ,
cust.birth_date as "date-of-birth",
DATE_FORMAT(cust.birth_date, '%m') AS "month-of-birth",
cust.gender as "gender",
COUNT(DISTINCT (CASE WHEN trans.order_date between '{Last_3_Days} 00:00:00' and '{END_DATE} 23:59:59' THEN trans.order_id END)) AS 'last-3-days-txn-count',
COUNT(DISTINCT (CASE WHEN trans.order_date between '{Last_7_Days} 00:00:00' and '{END_DATE} 23:59:59' THEN trans.order_id END)) AS 'last-7-days-txn-count',
COUNT(DISTINCT (CASE WHEN trans.order_date between '{Last_15_Days} 00:00:00' and '{END_DATE} 23:59:59' THEN trans.order_id END)) AS 'last-15-days-txn-count',
COUNT(DISTINCT (CASE WHEN trans.order_date between '{Last_30_Days} 00:00:00' and '{END_DATE} 23:59:59' THEN trans.order_id END)) AS 'last-30-days-txn-count',
COUNT(DISTINCT (CASE WHEN trans.order_date between '{Last_90_Days} 00:00:00' and '{END_DATE} 23:59:59' THEN trans.order_id END)) AS 'last-90-days-txn-count',
AVG(trans.total_amount) as "lifetime-avg-ticket-size",
SUM(trans.total_amount) as "lifetime-value",
COUNT(trans.order_id) as "lifetime-txn-count",
COUNT(DISTINCT (CASE WHEN info.promo_code is not null THEN trans.order_id END)) as "lifetime-promo-count",
MIN(trans.order_date) as "first-transaction",
(SELECT MAX(tb1.order_date) FROM ordering.order_transaction tb1 where tb1.user_id = cust.user_id AND tb1.order_status='complete' AND tb1.order_date < MAX(trans.order_date)) as "second-latest-transaction",
MAX(trans.order_date) as "latest-transaction",
COUNT(distinct shop.merchant_id) as "no-unique-merchants"
,"{ENV.DB_NAME}" as "_database"
FROM customer_profile.customer_profile cust 
left join ordering.order_transaction trans on cust.user_id = trans.user_id
left join ordering.order_transaction_info info on trans.order_id = info.order_id
left join merchant.shop shop on trans.shop_id = shop.shop_id
WHERE cust.user_id in (
        SELECT distinct trans.user_id
        FROM ordering.order_transaction trans 
        WHERE trans.order_date between '{START_DATE} 00:00:00' AND '{END_DATE} 23:59:59'
        and trans.order_status='COMPLETE')
and trans.order_status = 'COMPLETE'
and trans.order_date <= '{END_DATE} 23:59:59'
group by cust.user_id 

;
"""
iecho(query)
summary_cust = sql_pull(query, db='readonly')
# countdb_list.append(len(summary_cust))
summary_cust


# Update Customer2 COALESCE(
query = f"""
SELECT 
"update_customer" as "event",
custo.update_date as "created_at",
custo.user_id as "customer",
custo.register_date as "register-date",
MD5(custo.first_name) as "firstname",
MD5(custo.last_name) as "lastname",
CONCAT(MD5(custo.first_name),"@gmail.com") as "email",
CONCAT("+66",SUBSTRING(custo.mobile_no,2,5),"1234") as "sms" ,
custo.birth_date as "date-of-birth",
DATE_FORMAT(custo.birth_date, '%m') AS "month-of-birth",
custo.gender as "gender",
COALESCE(tbl.last_3_days_tx,0) as "last-3-days-txn-count",
COALESCE(tbl.last_7_days_tx,0) as "last-7-days-txn-count",
COALESCE(tbl.last_15_days_tx,0) as "last-15-days-txn-count",
COALESCE(tbl.last_30_days_tx,0) as "last-30-days-txn-count",
COALESCE(tbl.last_90_days_tx,0) as "last-90-days-txn-count",
COALESCE(tbl.lifetime_avg_ticket_size,0) as "lifetime-avg-ticket-size",
COALESCE(tbl.lifetime_value,0) as "lifetime-value",
COALESCE(tbl.lifetime_number_of_transaction,0) as "lifetime-txn-count",
COALESCE(tbl.lifetime_number_of_promos_used,0) as "lifetime-promo-count",
tbl.first_transaction_date as "first-transaction",
tbl.second_latest_transaction as "second-latest-transaction",
tbl.last_transaction_date as "latest-transaction",
COALESCE(tbl.total_of_unique_merchants_ordered,0) as "no-unique-merchants"
,"{ENV.DB_NAME}" as _database
FROM customer_profile.customer_profile custo
LEFT JOIN (
    SELECT  
    cust.user_id as customer,
    COUNT(DISTINCT (CASE WHEN trans.order_date between '{Last_3_Days} 00:00:00' and '{END_DATE} 23:59:59' THEN trans.order_id END)) AS 'last_3_days_tx',
    COUNT(DISTINCT (CASE WHEN trans.order_date between '{Last_7_Days} 00:00:00' and '{END_DATE} 23:59:59' THEN trans.order_id END)) AS 'last_7_days_tx',
    COUNT(DISTINCT (CASE WHEN trans.order_date between '{Last_15_Days} 00:00:00' and '{END_DATE} 23:59:59' THEN trans.order_id END)) AS 'last_15_days_tx',
    COUNT(DISTINCT (CASE WHEN trans.order_date between '{Last_30_Days} 00:00:00' and '{END_DATE} 23:59:59' THEN trans.order_id END)) AS 'last_30_days_tx',
    COUNT(DISTINCT (CASE WHEN trans.order_date between '{Last_90_Days} 00:00:00' and '{END_DATE} 23:59:59' THEN trans.order_id END)) AS 'last_90_days_tx',
    AVG(trans.total_amount) as lifetime_avg_ticket_size,
    SUM(trans.total_amount) as lifetime_value,
    COUNT(trans.order_id) as lifetime_number_of_transaction,
    COUNT(DISTINCT (CASE WHEN info.promo_code is not null THEN trans.order_id END)) as lifetime_number_of_promos_used,
    MIN(trans.order_date) as first_transaction_date,
    (SELECT MAX(trans.order_date) FROM ordering.order_transaction tb1 where tb1.user_id = cust.user_id AND tb1.order_status='complete' AND tb1.order_date < trans.order_date) as "second_latest_transaction",
    MAX(trans.order_date) as last_transaction_date,
    COUNT(distinct shop.merchant_id) as total_of_unique_merchants_ordered
    FROM customer_profile.customer_profile cust 
    left join ordering.order_transaction trans on cust.user_id = trans.user_id
    left join ordering.order_transaction_info info on trans.order_id = info.order_id
    left join merchant.shop shop on trans.shop_id = shop.shop_id
    WHERE cust.user_id in (
            SELECT  distinct cust.user_id
            FROM customer_profile.customer_profile cust
            WHERE  cust.update_date between '{START_DATE} 00:00:00' AND '{END_DATE} 23:59:59'
            AND cust.user_id not in (SELECT distinct trans.user_id
            FROM ordering.order_transaction trans 
            WHERE trans.order_date between '{START_DATE} 00:00:00' AND '{END_DATE} 23:59:59'
            AND trans.order_status='COMPLETE'))
    AND trans.order_status = 'COMPLETE'
    AND trans.order_date <= '{END_DATE} 23:59:59' 
    group by cust.user_id 
    ) tbl on custo.user_id = tbl.customer
WHERE custo.user_id in (
    SELECT  distinct cust.user_id
    FROM customer_profile.customer_profile cust
    WHERE (cust.update_date between '{START_DATE} 00:00:00' AND '{END_DATE} 23:59:59' or cust.register_date  between '{START_DATE} 00:00:00' AND '{END_DATE} 23:59:59')
    and cust.user_id not in (SELECT distinct trans.user_id
    FROM ordering.order_transaction trans 
    WHERE trans.order_date between '{START_DATE} 00:00:00' AND '{END_DATE} 23:59:59'
    and trans.order_status='COMPLETE'))
group by custo.user_id 

;
"""
iecho(query)
summary_cust2 = sql_pull(query, db='readonly')

customer_update=(pd.concat([summary_cust,summary_cust2]))
del summary_cust
del summary_cust2
customer_update


# start csv writer
# iecho(f'start csv writer : {output_path}/{transaction_file}')
# countdb_list.append(len(order_transaction))
order_transaction.to_csv(f'{output_path}/{transaction_file}', encoding='UTF-8', float_format='%.0f', quoting=csv.QUOTE_ALL, quotechar='"',
                    index=False, sep=",", escapechar='\\')

# iecho(f'start csv writer : {output_path}/{new_register_user_file}')
# countdb_list.append(len(new_user))
new_user.to_csv(f'{output_path}/{new_register_user_file}', encoding='UTF-8', float_format='%.0f', quoting=csv.QUOTE_ALL, quotechar='"',
                    index=False, sep=",", escapechar='\\')

# iecho(f'start csv writer : {output_path}/{update_customer_file}')
# countdb_list.append(len(customer_update))
customer_update.to_csv(f'{output_path}/{update_customer_file}', encoding='UTF-8',float_format='%.0f', quoting=csv.QUOTE_ALL, quotechar='"',
                    index=False, sep=",", escapechar='\\')



iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
iecho('                                EXPORT CSV                                   ')
iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")

def export_file(df_export=['']):
    number_file=1
    for i, df in enumerate(df_export):
        iecho(f'index         : {i}')
        # print(type(df))
        #iecho(f'data frame name : {df}')
        
        start = 0
        off_set = 50000

        data = df[start : off_set]
        # row = len(data)
        while len(data) != 0:
            iecho('off_set_start :'+str(start))
            iecho('off_set_end   :'+str(off_set))
            iecho('len data      :'+ str(len(data)))

            file_name = ENV.FILE_EXPORT.replace('xxx',str(number_file))
            iecho(f'start csv writer : {file_name}')
            data.to_csv(f'{output_path}/{file_name}', encoding='UTF-8', float_format='%.0f', quoting=csv.QUOTE_ALL, quotechar='"',
                    index=False, sep=",", escapechar='\\')

            list_filename.append(file_name)
            countdb_list.append(len(data))
            number_file+=1
            start=off_set
            off_set=off_set+50000
            data = df[start : off_set]

export_file([order_transaction,new_user,customer_update])


CORRECT_FILE = []
def validate_rowcount(list_filename, countdb_list):
    """
    Funcion validate number of row between (base table) and (export file)
    """
    iecho('validate rowcount')
    for i in range(len(list_filename)):

        filename = list_filename[i]
        COUNTBASETABLE = countdb_list[i]

        iecho(f'CONFIRM_COLUMN : {CONFIRM_COLUMN[i]}')
        if not CONFIRM_COLUMN[i]:
            iecho(f'FILENAME: {filename}')
            iecho(f'   ERROR: Can''t write control file becoure Count column missmach ')
            continue

        with open(f'{output_path}/{filename}', encoding="utf8") as f:
            for i, l in enumerate(f):
                pass
            ROWCOUNT=i

        if (COUNTBASETABLE != ROWCOUNT):
            iecho(f'FILENAME: {filename}')
            iecho(f'   ERROR: Record count on base table <> export data file')
            iecho(f'        :  Count base table = {COUNTBASETABLE}')
            iecho(f'        :  Count export file = {ROWCOUNT}')
            continue
        
        CORRECT_FILE.append(f'{output_path}/{str(filename)}')




def validate_column(list_filename):
    """
    Function validate column txt file
    """
    #append number of header column to list
    num_of_header = []
    global CONFIRM_COLUMN
    CONFIRM_COLUMN = []
    for file in list_filename:
        with open(f'{output_path}/{file}', encoding="utf8") as f:
            reader = csv.reader(f, delimiter=",", quoting=csv.QUOTE_ALL,
                                quotechar='"', escapechar='\\', skipinitialspace=True)
            first_row = next(reader)
            num_cols = len(first_row)
            iecho(f'{file}: {num_cols}')
            num_of_header.append(num_cols)
    num_of_header

    #validate number of data column
    for i in range(len(list_filename)):
        file = list_filename[i]
        num_header = num_of_header[i]

        with open(f'{output_path}/{file}', encoding="utf8") as f:
            reader = csv.reader(f, delimiter=",", quoting=csv.QUOTE_ALL,
                                quotechar='"', escapechar='\\', skipinitialspace=True)
            for row in reader:
                num_cols_data = len(row)
                if (num_cols_data != num_header):
                    iecho(f'FILENAME: {file}')
                    iecho(f'   ERROR: Column count on header <> column data')
                    iecho(f'        :  Count column on header = {num_header}')
                    iecho(f'        :  Count column on data = {num_cols_data}')
                    iecho(row)
                    CONFIRM_COLUMN.append(False)
                    break
        CONFIRM_COLUMN.append(True)


validate_column(list_filename)
validate_rowcount(list_filename, countdb_list)


def listfile():
    session = Session(aws_access_key_id=AWSAccessKeyId,
                      aws_secret_access_key=AWSSecretKey)
    s3 = session.resource('s3')
    bucket = s3.Bucket(BUCKET_NAME)
    files_in_s3 = bucket.objects.all()
    for file in files_in_s3:
        iecho(file.key)


def makefolder(foldername):
    s3 = boto3.client('s3')
    folder_name = foldername
    s3.put_object(Bucket=BUCKET_NAME, Key=(folder_name+'/'))


def uploadfile(content_file: None):
    session = Session(aws_access_key_id=AWSAccessKeyId,
                      aws_secret_access_key=AWSSecretKey)
    s3 = session.client('s3')

    for filename in content_file:
        key = "%s" % (os.path.basename(filename))
        iecho(f'Putting {filename} as {PATH_NAME}/{key}')
        s3.upload_file(filename, BUCKET_NAME, f'{PATH_NAME}/{key}')

    # for filename in content_file:
    #     os.remove(filename)


content_file = []
for file in CORRECT_FILE:
    apppath = str(Path().absolute())
    content_file.append(apppath+"/"+file)
# iecho(content_file)

iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
iecho('                            UPLOAD TO S3 BUCKET                              ')
iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
#uploadfile(content_file)

iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
iecho('                          LIST FILE FROM S3 BUCKET                           ')
iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
listfile()

iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
iecho('                                END PROCESS                                  ')
iecho("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
